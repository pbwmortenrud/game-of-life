class Cell {
  constructor(x, y, alive = true) {
    this.X = Number(x);
    this.Y = Number(y);
    this.alive = alive;

    this.coords = `${this.X}, ${this.Y}`;
  }

  getCoordinates() {
    return this.coords;
  }
  isAlive() {
    return this.alive;
  }
}

let cellArr = [];
let numberOfRowsCols = 5;
const numberOfCells = numberOfRowsCols ** 2;

function CreateCells(n) {
  cellArr = new Array(n);
  console.log(cellArr);
}



function PopulateCells() {
  i = 0;
  for (let y = 0; y < Math.sqrt(numberOfCells); y++) {
    for (let x = 0; x < Math.sqrt(numberOfCells); x++) {
      cellArr[i] = new Cell(x, y, true);
      i++;
    }
  }
  console.log(cellArr);
}

function draw() {
  for (const item of cellArr) {
    let cell = document.createElement("input");
    cell.type = "checkbox";
    cell.id = `x${item.X}y${item.Y}`;
    cell.value = item.alive;
    cell.checked = item.alive;    

    if (item.X == numberOfRowsCols - 1) {
      document.getElementById("grid").appendChild(cell);
      document.getElementById("grid").innerHTML += "<br>";
    } else document.getElementById("grid").appendChild(cell);
  }
}

// PLAY
CreateCells(numberOfCells);
PopulateCells();
draw();

//NEW TRY

function drawGrid(n) {
  let cell = document.createElement("input");
  cell.type = "checkbox";
  cell.id = `x${item.X}y${item.Y}`;
  cell.checked = item.alive;

  if (item.X == numberOfRowsCols - 1) {
    document.getElementById("grid").appendChild(cell);
    document.getElementById("grid").innerHTML += "<br>";
  } else document.getElementById("grid").appendChild(cell);
}
