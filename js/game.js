
// import { $, $ce } from "./domLib";

const $ = function (foo) {
  return document.getElementById(foo);
};

const $c = function (foo) {
  return document.getElementsByClassName(foo);
};

const $q = function (foo) {
  return document.querySelector(foo);
};

const $ce = function (foo) {
  return document.createElement(foo);
}


let cell = [];

let grid = [];

const size = 25;
let count = 1;

for (let i = 0; i < Math.sqrt(size); i++) {
  $('grid').appendChild($ce('tr'));
  for (let j = 0; j < Math.sqrt(size); j++) {
    cell = {'x': i, 'y': j, 'alive': true};
    grid.push(cell);
    document.write(`<td>${count}</td>`);
    count++;
  }
  document.write('</tr>')

}

console.table(grid);

console.log(grid[1]);


let o = {
  "x": 0,
  "y": 1
}

console.log(grid);