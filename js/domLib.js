const $ = function (foo) {
  return document.getElementById(foo);
};

const $c = function (foo) {
  return document.getElementsByClassName(foo);
};

const $q = function (foo) {
  return document.querySelector(foo);
};

const $ce = function (foo) {
  return document.createElement(foo);
}

const $qa = function (foo) {
  return document.querySelectorAll(foo);
};

export {$, $c, $q, $ce, $qa};