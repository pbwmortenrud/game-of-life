// "use strict";
// import * as domLib from "./domLib.js";

import { $q, $qa, $, $ce } from "./domLib.js";

// import { $, $c, $q, $qa, $ce } from "./domLib.js";
// const $ = function (foo) {
//   return document.getElementById(foo);
// };
// const $ce = function (foo) {
//   return document.createElement(foo);
// };
function make2DArray(cols, rows) {
  // let arr = new Array(cols).fill(new Array(rows));
  let arr = new Array(cols);
  for (let i = 0; i < arr.length; i++) {
    arr[i] = new Array(rows);
  }
  //console.log(arr);
  return arr;
}

// how to compute ID, currentRow, and currentColumn
function getId(currenRow, totalColumns, currentColumn) {
  if (arguments.length != 0) return currenRow * totalColumns + currentColumn;
  else return -1;
}
function getRow(id, totalColumns) {
  if (arguments.length != 0) return Math.floor(id / totalColumns);
  else return -1;
}
function getCol(id, totalColumns) {
  if (arguments.length != 0) return id % totalColumns;
  else return -1;
}
// console.log(getId(4, 12, 5));
// console.log(getCol(53, 12));
// console.log(getRow(53, 12));

//

let grid;
let next;
let width = 800;
let height = 550;
let resolution = 40;
//let resolution = $q('[data-resolution]').value;
let cols = Math.floor(width / resolution); //TODO: get from user input
let rows = Math.floor(height / resolution); //TODO: get from user input

function setResolution() {
  resolution = Number($q("[data-resolution]").value);
}
function setRowsAndCols() {
  cols = Math.floor(width / resolution);
  rows = Math.floor(height / resolution);
  // cols = Math.floor(Number($q("[data-x]").value) / resolution);
  // rows = Math.floor(Number($q("[data-y]").value) / resolution);
}

function random(n) {
  let number = Math.random() * n;
  return Math.floor(number);
}

function setup() {
  grid = make2DArray(cols, rows);
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      grid[i][j] = random(2);
    }
  }
  console.table(grid);
  next = make2DArray(cols, rows);
}

function drawGrid() {
  let cellgrid = $("cell-grid");
  // cellgrid.innerHTML = "";
  cellgrid.style.gridTemplateColumns = `repeat(${cols}, ${resolution}px)`;
  cellgrid.style.gridTemplateRows = `repeat(${rows}, ${resolution}px)`;
  let id = 0;
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      let cell = $ce("button");
      cell.setAttribute("data-cell", `${id}`);
      cellgrid.appendChild(cell);
      id++;
    }
  }
}

function countLiveNeighbors(grid) {
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      let state = grid[i][j];
      next[i][j] = state;

      //edges
      // if (i == 0 || i == cols - 1 || j == 0 || j == rows - 1) return;
      // else {
      let neighbors = countNeighbors(grid, i, j);
      // let neighbors = countLiveNeighborsById(getId(j, cols, i));
      if (state == 0 && neighbors == 3) {
        // console.log("you live");
        next[i][j] = 1;
      } else if (state == 1 && (neighbors < 2 || neighbors > 3)) {
        // console.log("you die!");
        next[i][j] = 0;
      } else {
        next[i][j] = grid[i][j];
      }
      // }
    }
  }
}
function countNeighbors(grid, x, y) {
  let sum = 0;

  for (let i = -1; i < 2; i++) {
    for (let j = -1; j < 2; j++) {
      // if (x + i < 0 || y + j < 0 || x + i > rows || y + j > cols) break;
      // else {
      //   sum += grid[x + i][y + j];
      // }
      if (x + i < 0 || y + j < 0 || y + j >= rows || x + i >= cols) {
        //do nothing
        continue;
        console.log("break i");
      }
      // else if (x + i > cols || y + j < 0) console.log("break 2");
      else {
        sum += grid[x + i][y + j];
      }
    }
  }
  sum -= grid[x][y];
  // console.log(`x:${x}, y:${y}, sum:${sum}`);
  return sum;
}
function countLiveNeighborsById(index) {
  let neighbors = 0;
  const x = getCol(index, cols);
  const y = getRow(index, cols);
  neighbors = countNeighbors(grid, x, y);
  console.log(`index:${index}, x:${x}, y:${y}, sum:${neighbors}`);
  return neighbors;
}

function toggleLifeById(index) {
  toggleClassDead(index);
  changeState(index);
}

function changeState(index) {
  const x = getCol(index, cols);
  const y = getRow(index, cols);

  let state = grid[x][y];
  if (state === 1) state = 0;
  else state = 1;

  grid[x][y] = state;
}

function nextCycle() {
  countLiveNeighbors(grid);
  updateGrid(next);
  grid = next;
  next = make2DArray(cols, rows);
}

let timer = 0;
function startTimer(ms) {
  if (timer === 0) {
    timer = window.setInterval(function () {
      nextCycle();
    }, ms);
  }
}
function stopTimer(intervalId) {
  clearInterval(intervalId);
  timer = 0;
}

function toggleClassDead(index) {
  $q(`[data-cell="${index}"]`).classList.toggle("dead");
}

function setCycleClickEvent() {
  $q("[data-cycle]").addEventListener("click", () => {
    nextCycle();
  });
}

function setStartClickEvent() {
  $q("[data-start]").addEventListener("click", () => {
    startTimer(200);
  });
}
function setStopClickEvent() {
  $q("[data-stop]").addEventListener("click", () => {
    stopTimer(timer);
  });
}

function setClearClickEvent() {
  $q("[data-clear]").addEventListener("click", () => {
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        let cleared = 0;
        grid[i][j] = cleared;
      }
    }
    updateGrid(grid);
  });
}
function setNewClickEvent() {
  $q("[data-new]").addEventListener("click", () => {
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        grid[i][j] = random(2);
      }
    }
    updateGrid(grid);
  });
}

function setCellClickEvent() {
  $qa("[data-cell").forEach((cell, index) => {
    cell.addEventListener("click", () => {
      toggleLifeById(index);
      countLiveNeighborsById(index);
    });
  });
}

function updateGrid(grid) {
  $qa("[data-cell").forEach((cell, index) => {
    cell.classList.remove("dead");
    if (grid[getCol(index, cols)][getRow(index, cols)] == "0")
      cell.classList.add("dead");
  });
}

//Execution
setup();
// setResolution();
// setRowsAndCols();
drawGrid();
setCellClickEvent();
setCycleClickEvent();
setClearClickEvent();
setNewClickEvent();
setStartClickEvent();
setStopClickEvent();
updateGrid(grid);

//Try automation
// let intervalId = window.setInterval(function(){
//   nextCycle();
// }, 250);

// console.log(countLiveNeighborsById(26));
